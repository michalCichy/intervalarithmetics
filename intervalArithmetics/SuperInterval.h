#pragma once
#include "stdafx.h"
#include "IntervalUtils.h"

class SuperInterval {
public:
	mpreal left = 0;
	mpreal right = 0;

	void setRounding(mpfr_rnd_t mode) {
		mpreal::set_default_rnd(mode);
	}

	static SuperInterval convert(string value) {
		mpreal left = mpreal(value, mpreal::get_default_prec(), 10, DOWN);
		mpreal right = mpreal(value, mpreal::get_default_prec(), 10, UP);
		
		return SuperInterval(left, right);
	}

	SuperInterval(mpreal value) {
		left = value;
		right = value;
	}

	SuperInterval(mpreal left, mpreal right) {
		this->left = left;
		this->right = right;
	}

	SuperInterval operator + (SuperInterval element) {
		setRounding(DOWN);
		mpreal outputLeft = this->left + element.left;
		setRounding(UP);
		mpreal outputRight = this->right + element.right;

		return SuperInterval(outputLeft, outputRight);
	}

	SuperInterval operator - (SuperInterval element) {
		setRounding(DOWN);
		mpreal outputLeft = left - element.right;
		setRounding(UP);
		mpreal outputRight = right - element.left;

		return SuperInterval(outputLeft, outputRight);
	}

	SuperInterval multiply(SuperInterval element) {
		setRounding(DOWN);
		mpreal outputLeft = min(this->left * element.left,
			this->left * element.right,
			this->right * element.left,
			this->right * element.right);
		setRounding(UP);
		mpreal outputRight = max(this->left * element.left,
				this->left * element.right,
				this->right * element.left,
				this->right * element.right);

		return SuperInterval(outputLeft, outputRight);
	}

	SuperInterval operator * (SuperInterval element) {
		return multiply(element);
	}

	SuperInterval operator / (SuperInterval element) {
		if (element.contains(0)) {
			cout << "UWAGA! Dzielenie przez zero!";
		}

		setRounding(DOWN);
		mpreal outputLeft = 1 / element.right;
		setRounding(UP);
		mpreal outputRight = 1 / element.left;

		SuperInterval multiplier(outputLeft, outputRight);

		return multiply(multiplier);
	}

	void makeAbs() {
		if (left < 0) {
			left = -left;
		}
		if (right < 0) {
			right = -right;
		}
	}

	bool contains(mpreal value) {
		return (value >= left && value <= right);
	}

	bool intersects(SuperInterval other) {
		return this->contains(other.left) || this->contains(other.right) || other.contains(left);
	}

	SuperInterval& operator = (mpreal value) {
		this->left =  value;
		this->right = value;
		this->left.set_default_rnd(DOWN);
		this->right.set_default_rnd(UP);

		return *this;
	}

	SuperInterval operator+(mpreal element) {
		SuperInterval s(element);
		return operator+(s);
	}

	SuperInterval operator*(mpreal element) {
		SuperInterval s(element);
		return operator*(s);
	}

	SuperInterval operator/(mpreal element) {
		SuperInterval s(element);
		return operator/(s);
	}

	SuperInterval operator-(mpreal element) {
		SuperInterval s(element);
		return operator-(s);
	}

	bool operator<(SuperInterval element) {
		return right < element.left;
	}

	bool operator>(SuperInterval element) {
		return left > element.right;
	}

	void print() {
		cout << std::setprecision(100) << "[ " << left << ",\n  " << right << " ]\n";
	}
};

