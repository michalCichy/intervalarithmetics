#pragma once
#include <iostream>
#include <iomanip>
#include <vector>
#include <mpreal.h>
#include <fstream>
#include "Interval.h"
#include <string>
//#include "Interval.h"

#define DOWN MPFR_RNDD
#define UP MPFR_RNDU
using mpfr::mpreal;
using std::cout;
using std::string;
using std::vector;
using std:: ofstream;

#define interval interval_arithmetic::Interval<long double>