#pragma once
#include "stdafx.h"

void print(mpreal number) {
	std::cout << std::setprecision(100) << number << '\n';
}

mpreal min(mpreal a, mpreal b, mpreal c) {
	return min(min(a, b), c);
}

mpreal min(mpreal a, mpreal b, mpreal c, mpreal d) {
	return min(min(a, b), min(c,d));
}

mpreal max(mpreal a, mpreal b, mpreal c) {
	return max(max(a, b), c);
}

mpreal max(mpreal a, mpreal b, mpreal c, mpreal d) {
	return max(max(a, b), max(c, d));
}
mpreal ABSX(mpreal value) {
	if (value < 0) value *= -1;
	return value;
}