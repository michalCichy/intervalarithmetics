#include "stdafx.h"
#include "IntervalUtils.h"
#include "BinaryRoot.h"
#include "SuperInterval.h"

void init() {
	const int digits = 50;
	mpreal::set_default_prec(mpfr::digits2bits(digits));
}

int main(int argc, char* argv[])
{
	init();

	if (argc >= 5) {
		interval left(interval::LeftRead(argv[1]), interval::RightRead(argv[1]));
		interval right(interval::LeftRead(argv[2]), interval::RightRead(argv[2]));
		interval precision(atof(argv[3]), atof(argv[3]));
		int iterationsCount = atoi(argv[4]);

		vector <interval> coeffs;

		for (int i = 5; i < argc; i+=2) {
			coeffs.push_back(interval(interval::LeftRead(argv[i]), interval::RightRead(argv[i + 1])));
		}

		interval value;
		interval result  = binaryRoot(coeffs, left, right, precision, iterationsCount, value);

		ofstream out;
		out.open("wynik");

		//string leftOutput, rightOutput, vl, vr;
		//result.IEndsToStrings(leftOutput, rightOutput);
		//value.IEndsToStrings(vl, vr);
		cout << std::scientific << std::setprecision(14) << result.a << '\n' << result.b << '\n' << iterationsCount + 1<< '\n' << value.a << value.b;
		out << std::scientific << std::setprecision(14) << result.a << '\n' << result.b << '\n' << iterationsCount + 1<< '\n' << value.a << value.b;
		out.close();
	}
	else {
		vector<interval> coeffs{ interval(1,1),
			interval(2,2),
			interval(3, 3)};
		interval value;
		int iterations = 100;
		interval result = binaryRoot(coeffs, interval(0,0), interval(1,1), interval(0.01, 0.01), iterations, value);
		cout << std::scientific << result.a << "\n" << result.b;
		string leftOutput, rightOutput, vl, vr;
		//result.IEndsToStrings(leftOutput, rightOutput);
		//value.IEndsToStrings(vl, vr);
		//cout << std::setprecision(100) << leftOutput << '\n' << rightOutput << '\n' << iterations << '\n' << vl << vr;
	}
	
	return 0;
}


