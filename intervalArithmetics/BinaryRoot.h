#pragma once
#include "stdafx.h"
#include "SuperInterval.h"

interval polynomialValue(std::vector <interval>& coefficients, interval value) {

	interval sum = coefficients[0];

	int index = 1;

	while (index < coefficients.size()) {
		sum = sum * value + coefficients[index];
		index++;
	}

	return sum;
}

bool differentSigns(interval v) {
	return (v.a <= 0 && v.b >= 0);
}

int iterations = -1;

interval binaryRoot(std::vector <interval> coefficients, interval left, interval right, interval epsilon, int& iterationsCount, interval& value ) {	
	iterations++;
	std::cout << "ITERATION\n";
	interval middle = (right + left) / interval(2,2);

	interval middleValue = polynomialValue(coefficients, middle);
	
	//std::cout << middle.left << " | " << polyValue.left << "        [" << left.left << " -- " << right.right <<"]\n";
	/*SuperInterval absPolyValue = polyValue;
	absPolyValue.makeAbs();*/
	//SuperInterval leftRight(left.left, right.right);

	interval leftValue = polynomialValue(coefficients, left);
	interval rightValue = polynomialValue(coefficients, middle);

	/*if (differentSigns(leftValue) || differentSigns(rightValue)) {
		iterationsCount = -1;

		long double x, y;
		if (leftValue.a < rightValue.a) {
			x = leftValue.a;
		}
		else {
			x = leftValue.a;
		}
		if (leftValue.b < rightValue.b) {
			y = rightValue.b;
		}
		else {
			y = leftValue.b;
		}
		value = interval(x, y);
		return interval(left.a, right.b);
	}*/

	if (right - left < epsilon || iterations >= iterationsCount) {
		iterationsCount = iterations;
		
		long double x, y;
		if (leftValue.a < rightValue.a) {
			x = leftValue.a;
		}
		else {
			x = leftValue.a;
		}
		if (leftValue.b < rightValue.b) {
			y = rightValue.b;
		}
		else {
			y = leftValue.b;
		}
		value = interval(x, y);
		return interval(left.a, right.b);
	}
	else { 
		if (interval(0,0) < (leftValue * rightValue)) {
			//std::cout << "Biore PRAWO\n";
			interval root = binaryRoot(coefficients, middle, right, epsilon, iterationsCount, value);
			return root;
		}
		else {
			//std::cout << "Biore LEWO\n";
			interval root = binaryRoot(coefficients, left, middle, epsilon, iterationsCount, value);
			return root;
		}
	}
}
